import axios from "axios";

class CountryService {
  getCountries() {
    return axios.get("https://restcountries.eu/rest/v2/all");
  }
}

export default new CountryService();
